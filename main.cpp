/**
 * @author: raida <raida@crans.org>
 * @date:   Creation: 09/12/2018
 *          Last update: 20/05/2019
 */

#include <iostream>
#include <thread>
#include "Detector.h"
#include "Position.h"
#include "Tools.h"

using namespace std;
using namespace cv;


void detect(bool, string, bool, bool);
void help(const char*);


/**
 * Parse les paramètres d'entré.
 *
 * @param argc [int]: Nombre de paramètres
 * @param argv [char**]: Tableau contenant le string de chaque paramètre
 *
 * @return [int]: 0 si succès sinon autre chose
 */
int main(int argc, char** argv) {
    bool display = false;
    bool display_data = false;
    bool send_data = false;
    string entry = DEFAULT_ENTRY;

    string opt;
    int i(1);
    const char* prog = argv[0];

    while(argv[i]) {
        opt = argv[i];
        if (opt == "-h" || opt == "--help") {
            help(prog);
            return EXIT_SUCCESS;
        } else if (opt == "-d" || opt == "--display") {
            display = true;
        } else if (opt == "-e" || opt == "--entry") {
            entry = argv[++i];
        } else if (opt == "-s" || opt == "--send-data") {
            send_data = true;
        } else if (opt == "--display-data") {
            display_data = true;
        } else {
            cout << "Option inconnue : " << opt << endl;
            cout << "Exécutez « " << prog << " --help » pour plus de renseignements." << endl;
            return EXIT_FAILURE;
        }
        i++;
    }

    detect(display, entry, send_data, display_data);
    return EXIT_SUCCESS;
}


/**
 * Programme principal après application des paramètres d'entrés
 *
 * @param display [bool]: True si on souhaite afficher la caméra sinon False
 * @param entry [string]: Flux d'entrée de la vidéo. Par défaut il s'agit de la caméra
 * @param send_data [bool]: True si on souhaite récupérer les données en temps réel
 * @param display_data [bool]: True si on souhaite afficher les données en temps réel
 * @param python_module [string]: Nom du module python à charger
 */

void detect(bool display, string entry, bool send_data, bool display_data) {
    Detector detector(display, entry, send_data);
    // Pointeur afin de récupérer les positions pendant que le programme tourne.
    PositionArray* pPositionArray = new PositionArray(vector<Position>());
    
    // Thread gérant le détecteur.
    thread t1([&detector, pPositionArray]() {
        detector.detect(pPositionArray);
    });

    // Thread gérant la récupération et le traitement des données
    thread t2([&detector, pPositionArray, &display_data]() {
        int currentPositionsId = -1;
        while (!detector.ended) {
            if (pPositionArray->getId() != currentPositionsId) {
                currentPositionsId = pPositionArray->getId();

                //////////////////////////////////////////////////////////
                ///                    Zone to edit                    ///
                //////////////////////////////////////////////////////////

                vector<Position> positions = pPositionArray->getPositions();

                if (display_data) {
                    stringstream ss;
                    ss << "[Positions]" << endl;
                    for (const auto &position : positions) {
                        ss << '\t' << position << endl;
                    }
                    log(ss.str());
                }

                /////////////////////////////////////////////////
                ///                    End                    ///
                /////////////////////////////////////////////////
            }
        }
    });

    t1.join();
    t2.join();
}


/**
 * Affiche un message d'aide pour les paramètres.
 *
 * @param prog [char*]: Nom du programme
 */
void help(const char* prog) {
    cout << "Utilisation :\t" << prog << " [-d|--display] [[-e|--entry] <file_name>] [[-p|--python-module] <file_name>] [-s|--send-data] [--display-data]" << endl;
    cout << "\t\t" << prog << " [-h|--help]" << endl << endl;
    cout << endl << "OPTIONS :" << endl;
    cout << "\t-d, \t--display" << endl << "\t\tActive l'affiche de la caméra" << endl << endl;
    cout << "\t-e, \t--entry, \t\t<file_name>" << endl << "\t\tChange l'entrée par défaut" << endl << endl;
    cout << "\t-s, \t--send-data" << endl << "\t\tActive l'envoie des données en temps réel" << endl << endl;
    cout << "\t--display-data" << endl << "\t\tActive l'affiche des données en temps réel" << endl << endl;
    cout << "\t-h, \t--help" << endl << "\t\tAffiche cette aide" << endl << endl;
}
