/**
 * @author: raida <raida@crans.org>
 * @date:   Creation: 14/12/2018
 *          Last update: 28/04/2019
 */

#ifndef V_CPP_TOOLS_H
#define V_CPP_TOOLS_H

#include <iostream>
#include <vector>

double mean(std::vector<double>);
std::string getCurrentDateTime(std::string);
void log(std::string, bool=false);

#endif //V_CPP_TOOLS_H
