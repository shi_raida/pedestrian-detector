/**
 * @author: raida <raida@crans.org>
 * @date:   Creation: 09/12/2018
 *          Last update: 14/05/2019
 */

#ifndef V_CPP_DETECTOR_H
#define V_CPP_DETECTOR_H

#ifndef DEFAULT_ENTRY
#define DEFAULT_ENTRY "camera"
#endif //DEFAULT_ENTRY

#include <iostream>
#include <mutex>
#include <vector>
#include "opencv2/opencv.hpp"
#include "PositionArray.h"
#include "Region.h"


/**
 * Représente le détecteur de personnes. Il s'agit du coeur du programme.
 *
 * @param ended [bool]: True si la détection est finie sinon False
 */
class Detector {
public:
    explicit Detector(bool, std::string, bool);
    ~Detector();
    void detect(PositionArray*);

    bool ended;

private:
    void createMask();
    void dilation();
    void erosion();
    void fillRegion(const cv::Point&);
    void filterRegions();
    void findRegions();
    void initializeFinderMask();
    void read();
    void sendData();
    void show();

    // Limitation de la région
    const int regionMaskV = 1;              // Taille verticale du masque de recherche
    const int regionMaskH = 1;              // Taille horizontal du masque de recherche
    const int regionStepV = 1;              // Pas vertical lors de la création du masque de recherche
    const int regionStepH = 1;              // Pas horizontal lors de la création du masque de recherche
    const cv::Rect zone = cv::Rect(cv::Point(0, 0), cv::Point(399, 300));   // Rectangle de la taille de l'image

    // Limitation de l'ouverture morphologique
    const int openingV = 2; // Taille verticale du masque de l'ouverture
    const int openingH = 2; // Taille horizontale du masque de l'ouverture

    // Filtrage de la région
    const float pixelLimit = 230.;  // Valeur de détection des pixels
    const int xEps = 50;            // Epsilon pour la combinaison de plusieurs régions
    const int sizeLimit = 20;       // Nombre minimal de pixels dans une région

    // Paramètres pour l'affichage
    const cv::Scalar white = cv::Scalar(255, 255, 255); // Blanc
    const cv::Scalar black = cv::Scalar(0  , 0  , 0  ); // Noir
    const cv::Scalar red =   cv::Scalar(0  , 0  , 255); // Rouge
    const cv::Scalar green = cv::Scalar(0  , 255, 0  ); // Vert
    const cv::Scalar blue =  cv::Scalar(255, 0  , 0  ); // Bleu
    const int font = cv::FONT_HERSHEY_COMPLEX_SMALL;    // Police d'écriture sur les rendus

    // Entrées caméra et utilisateur
    cv::VideoCapture camera;                // Camera
    cv::Mat frame;                          // Frame courante
    PositionArray* positions{};             // Pointeur permettant de transmettre l'ensemble des positions en parallèle
    int keyboard;                           // Entrée du clavier
    bool display;                           // Affichage de la caméra en direct
    bool send_data;                         // Envoie les données en temps réel ou non

    // Frames
    cv::Mat background;                     // Arriere plan du detecteur
    cv::Mat detectionFrame;                 // Image après détection mais avant fusion
    cv::Mat erosionFrame;                   // Image après l'érosion
    cv::Mat fgMaskKNN;                      // Masque d'arrière plan généré par une méthode KNN
    cv::Mat filteredFrame;                  // Masque d'arrière plan filtré
    cv::Mat origin;                         // Image originale

    // Background subtractor
    cv::Ptr<cv::BackgroundSubtractorKNN> pKNN; // Soustracteur de fond KNN
    const int history = 5000;               // Taille de l'historique
    const double dist2Threshold = 1100.0;   // Distance entre le pixel et l'échantillon pour décider s'il est background

    // Masques et régions
    std::vector<std::vector<bool>> finderMask;  // Masque traçant les pixels déjà visités
    std::vector<cv::Point> mask;                // Masque de recherche
    std::vector<cv::Point2f> regions;           // Ensemble des régions potentielles

    // Timers
    std::vector<double> erosionTimer;   // Timer pour la fonction d'érosion
    std::vector<double> findTimer;      // Timer pour la fonction de recherche
    std::vector<double> filterTimer;    // Timer pour la fonction de filtre
    std::vector<double> readTimer;      // Timer pour la fonction de lecture de la caméra
    std::vector<double> showTimer;      // Timer pour la fonction d'affichage
};


#endif //V_CPP_DETECTOR_H
