/**
 * @author: raida <raida@crans.org>
 * @date:   Creation: 09/12/2018
 *          Last update: 14/05/2019
 */

#include "Detector.h"
#include "Tools.h"
#include <sstream>

using namespace std;
using namespace cv;


/**
 * @param display [bool]: True si on souhaite afficher la caméra sinon False
 * @param entry[string]: Flux d'entrée de la vidéo. Par défaut il s'agit de la caméra
 * @param send_data[bool]: True si on souhaite récupérer les données en temps réel
 */
Detector::Detector(bool display, string entry, bool send_data) {
    if (entry == DEFAULT_ENTRY) {
        this->camera = VideoCapture(0);
    } else {
        this->camera = VideoCapture(entry);
    }
    if(!camera.isOpened()) {
        stringstream ss;
        if (entry == DEFAULT_ENTRY) {
            ss << "[ERROR] Unable to have camera access." << endl << "Exiting..." << endl;
        } else {
            ss << "[ERROR] Unable to have access to this file : «" << entry << "»." << endl << "Exiting..." << endl;
        }
        log(ss.str(), true);
        exit(EXIT_FAILURE);
    }

    this->pKNN = createBackgroundSubtractorKNN(this->history, this->dist2Threshold, false);
    this->keyboard = 0;
    this->createMask();

    this->ended = false;
    this->display = display;
    this->send_data = send_data;
}


Detector::~Detector() {
    pKNN.release();
    this->camera.release();
    destroyAllWindows();
}


/**
 * Boucle principale de détection.
 *
 * @param positions [PositionArray*]: Pointeur permettant de transmettre l'ensemble des positions en parallèle
 */
void Detector::detect(PositionArray* positions) {
    this->positions = positions;
    vector<double> loopTimer;
    vector<double> applyTimer;
    int64 t1 = getTickCount();
    int nbFrame = 0;

    while((char)this->keyboard != 'q' && (char)this->keyboard != 27) {
        int64 t2 = getTickCount();
        nbFrame++;
        this->read();
        this->origin = this->frame.clone();

        int64 t3 = getTickCount();
        this->pKNN->apply(this->frame, this->fgMaskKNN);
        applyTimer.push_back((getTickCount() - t3) / getTickFrequency());

        if (nbFrame > 100) {  // Attente de mise en données du fond
            this->filteredFrame = this->fgMaskKNN.clone();
            this->erosion();

            this->findRegions();
            this->filterRegions();

            if (this->send_data)
                this->sendData();

            if (this->display)
                this->show();
        }
        this->keyboard = waitKey(10);
        loopTimer.push_back((getTickCount() - t2) / getTickFrequency());

        if (this->keyboard == 32) {
            while (true) {
                this->keyboard = waitKey(10);
                if (this->keyboard == 32) {
                    break;
                }
            }
        }
    }

    this->ended = true;
    stringstream ss;

    ss << "[INFO] Temps total: " << (getTickCount() - t1) / getTickFrequency() << "s" << endl;
    ss << "\tloop: " << mean(loopTimer) << "s" << endl;
    ss << "\tread: " << mean(this->readTimer) << "s - " << mean(this->readTimer) / mean(loopTimer) * 100 << "%" << endl;
    ss << "\tapply: " << mean(applyTimer) << "s - " << mean(applyTimer) / mean(loopTimer) * 100 << "%" << endl;
    ss << "\terosion: " << mean(this->erosionTimer) << "s - " << mean(this->erosionTimer) / mean(loopTimer) * 100 << "%" << endl;
    ss << "\tfind: " << mean(this->findTimer) << "s - " << mean(this->findTimer) / mean(loopTimer) * 100 << "%" << endl;
    ss << "\tfilter: " << mean(this->filterTimer) << "s - " << mean(this->filterTimer) / mean(loopTimer) * 100 << "%" << endl;
    ss << "\tshow: " << mean(this->showTimer) << "s - " << mean(this->showTimer) / mean(loopTimer) * 100 << "%" << endl;
    log(ss.str());
}


/**
 * Créer le masque de recherche
 */
void Detector::createMask() {
    this->mask.clear();
    for(int x = -this->regionMaskV; x <= this->regionMaskV; x += this->regionStepV) {
        for(int y = -this->regionMaskH; y <= this->regionMaskH; y += this->regionStepH) {
            this->mask.emplace_back(Point(x, y));
        }
    }
}


void Detector::erosion() {
    int64 t = getTickCount();

    blur(this->filteredFrame, this->filteredFrame, Size(5, 5));
    this->erosionFrame = this->filteredFrame.clone();

    this->erosionTimer.push_back((getTickCount() - t) / getTickFrequency());
}


/**
 * Filtre les régions pour ne garder que les plus pertinentes.
 * Pour cela on commence par supprimer les régions trop petites, puis les régions horizontales, puis on combine les
 * régions de manière verticale (problème de découpage de la personne à la taille).
 */
void Detector::filterRegions() {
    for (int i = 0; i < this->regions.size(); i++) {
        stringstream ss;
        Point2f pos = this->regions[i];

        line(this->detectionFrame, Point(pos.x - 5, pos.y), Point(pos.x + 5, pos.y), this->white);
        line(this->detectionFrame, Point(pos.x, pos.y - 5), Point(pos.x, pos.y + 5), this->white);
    }

    int64 t = getTickCount();
    Point2f r1, r2;

    // Combinaison
    for(int i = 0; i < this->regions.size(); i++) {
        for(int j = 0; j < this->regions.size(); j++) {
            if(i == j) continue;
            r1 = this->regions[i];
            r2 = this->regions[j];
            if(r1.x > r2.x - this->xEps && r1.x < r2.x + this->xEps) {
                this->regions.push_back(Point2f((r1.x + r2.x)/2, (r1.y + r2.y)/2));
                this->regions.erase(this->regions.begin()+i);
                if (i > j)
                    this->regions.erase(this->regions.begin()+j);
                else
                    this->regions.erase(this->regions.begin()+j-1);
                i = 0;
                j = 0;
            }
        }
    }

    // Suppression hors champs
    for(int i = 0; i < this->regions.size(); i++) {
        Point2f r = this->regions[i];
        if(r.x < 0 || r.y < 0) {
            this->regions.erase(this->regions.begin()+i);
            i = 0;
        }
    }

    this->filterTimer.push_back((getTickCount() - t) / getTickFrequency());
}


/**
 * Trouve l'ensemble des régions
 */
void Detector::findRegions() {
    int64 t = getTickCount();

    threshold(this->filteredFrame, this->detectionFrame, 240., 255., 0);

    Mat canny_output;
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    /// Detect edges using canny
    Canny(this->detectionFrame, canny_output, 100, 200, 3 );
    /// Find contours
    findContours( canny_output, contours, hierarchy, 3, 2, Point(0, 0) );

    /// Get the moments
    vector<Moments> mu(contours.size() );
    for( int i = 0; i < contours.size(); i++ )
    { mu[i] = moments( contours[i], false ); }

    ///  Get the mass centers:
    this->regions = vector<Point2f>( contours.size() );
    for( int i = 0; i < contours.size(); i++ )
    { this->regions[i] = Point2f( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 ); }

    this->findTimer.push_back((getTickCount() - t) / getTickFrequency());
}


/**
 * Initialise le masque de détection
 */
void Detector::initializeFinderMask() {
    if(this->finderMask.empty()) {
        vector<bool> row;
        for(int j = 0; j < this->filteredFrame.rows; j++) {
            row.push_back(false);
        }
        for(int i = 0; i < this->filteredFrame.cols; i++) {
            this->finderMask.emplace_back(row);
        }
    } else {
        for(int i = 0; i < this->filteredFrame.cols; i++) {
            for(int j = 0; j < this->filteredFrame.rows; j++) {
                this->finderMask[i][j] = false;
            }
        }
    }
}


/**
 * Lit l'entrée caméra
 */
void Detector::read() {
    int64 t = getTickCount();
    if(!this->camera.read(this->frame)) {
        stringstream ss;
        ss << "[ERROR] Unable to read next frame." << endl << "Exiting..." << endl;
        log(ss.str(), true);
        exit(EXIT_FAILURE);
    }
    resize(this->frame, this->frame, Size(399, 300));
    this->readTimer.push_back((getTickCount() - t) / getTickFrequency());
}


/**
 * Envoie les données actuelles
 */
void Detector::sendData() {
    vector<Point2f> positions = vector<Point2f>();
    for (auto &region : this->regions) {
        positions.push_back(region);
    }

    vector<Position> datas = vector<Position>();
    for (int i = 0; i < positions.size(); i++) {
        datas.emplace_back(positions[i], 0);
    }
    this->positions->setPositions(datas);
}


/**
 * Affiche la caméra et le résultat du traitement
 */
void Detector::show() {
    int64 t = getTickCount();

    for (auto &pos : this->regions) {
        stringstream ss;

        line(this->filteredFrame, Point(pos.x - 5, pos.y), Point(pos.x + 5, pos.y), this->white);
        line(this->filteredFrame, Point(pos.x, pos.y - 5), Point(pos.x, pos.y + 5), this->white);

        line(this->frame, Point(pos.x - 5, pos.y), Point(pos.x + 5, pos.y), this->green);
        line(this->frame, Point(pos.x, pos.y - 5), Point(pos.x, pos.y + 5), this->green);

        ss << "(" << pos.x << ", " << pos.y << ")";
        putText(this->filteredFrame, ss.str(), pos + Point2f(0, -5), this->font, 0.6, this->white);
        putText(this->frame, ss.str(), pos + Point2f(0, -5), this->font, 0.6, this->green);
    }

    /*
    resize(this->frame, this->frame, Size(800, 600));
    resize(this->fgMaskKNN, this->fgMaskKNN, Size(800, 600));
    resize(this->filteredFrame, this->filteredFrame, Size(800, 600));
    */

    this->pKNN->getBackgroundImage(this->background);

    imshow("Origin", this->origin);
    imshow("Background", this->background);
    imshow("Separation", this->fgMaskKNN);
    imshow("Erosion", this->erosionFrame);
    if (this->detectionFrame.cols != 0)  // Bug au début où les dimensions sont nulles
        imshow("Detection", this->detectionFrame);
    imshow("Fusion", this->filteredFrame);
    imshow("Final", this->frame);

    this->showTimer.push_back((getTickCount() - t) / getTickFrequency());
}
