/**
 * @author: raida <raida@crans.org>
 * @date:   Creation: 08/03/2019
 *          Last update: 08/03/2019
 */

#include "PositionArray.h"

using namespace std;


/**
 * @param positions [vector<Position>]: Ensemble des positions
 */
PositionArray::PositionArray(vector<Position> positions) {
    this->id = 0;
    this->positions = move(positions);
}


/**
 * @return [int]: Identifiant de l'ensemble de positions
 */
int PositionArray::getId() const {
    return this->id;
}


/**
 * @return [vector<Position>]: Ensemble des positions
 */
vector<Position> PositionArray::getPositions() const {
    return this->positions;
}


/**
 * Change l'ensemble des positions et met à jour l'identifiant.
 *
 * @param positions [vector<Position>]: Nouvel ensemble
 */
void PositionArray::setPositions(vector<Position> positions) {
    this->positions = move(positions);
    this->id++;
}