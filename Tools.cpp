/**
 * @author: raida <raida@crans.org>
 * @date:   Creation: 14/12/2018
 *          Last update: 28/04/2019
 */

#include <fstream>
#include "Tools.h"

using namespace std;


std::string logfileName;


/**
 * @param v [vector<double>]: Liste de réels
 * @return [double]: Moyenne des réels de v
 */
double mean(vector<double> v) {
    double s = 0;
    for(auto& d : v)
        s += d;
    return s / v.size();
}


/**
 * @return [string]: La date et l'heure actuelle sous la forme 2019-04-28_11_01_38
 */
string getCurrentDateTime(){
    time_t now = time(nullptr);
    struct tm  tstruct{};
    char  buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d_%H_%M_%S", &tstruct);
    return string(buf);
};


/**
 * Affiche un message en console et l'enregistre dans un fichier de log
 *
 * @param logMsg [string]: Message à afficher
 * @param error [bool]: True s'il s'agit d'un message d'erreur sinon False
 */
void log(string logMsg, bool error){
    if (logfileName.empty())
        logfileName = getCurrentDateTime();

    string filePath = "log/" + logfileName + ".log";
    ofstream ofs(filePath.c_str(), std::ios_base::out | std::ios_base::app);

    ofs << logMsg << endl;
    if (error)  cerr << logMsg;
    else        cout << logMsg;

    ofs.close();
}