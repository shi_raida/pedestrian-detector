/**
 * @author: raida <raida@crans.org>
 * @date:   Creation: 08/03/2019
 *          Last update: 08/03/2019
 */

#ifndef PEDESTRIAN_DETECTOR_POSITIONARRAY_H
#define PEDESTRIAN_DETECTOR_POSITIONARRAY_H

#include "Position.h"


/**
 * Représente un ensemble de positions portant un identifiant afin de différencier les ensembles.
 *
 * @param id [int] : Identifiant de l'ensemble
 * @param positions [vector<Position>} : Ensemble des positions
 */
class PositionArray {
public:
    explicit PositionArray(std::vector<Position>);
    int getId() const;
    std::vector<Position> getPositions() const;
    void setPositions(std::vector<Position>);

private:
    int id;
    std::vector<Position> positions;
};


#endif //PEDESTRIAN_DETECTOR_POSITIONARRAY_H
