/**
 * @author: raida <raida@crans.org>
 * @date:   Creation: 11/01/2019
 *          Last update: 28/04/2019
 */

#ifndef PEDESTRIAN_DETECTOR_POSITION_H
#define PEDESTRIAN_DETECTOR_POSITION_H

#include <vector>
#include "opencv2/opencv.hpp"


/**
 * Représente la position d'un individu dans l'espace.
 *
 * @param x [int]: Position horizontale
 * @param y [int]: Position verticale
 * @param z [int]: Profondeur
 */
class Position {
public:
    explicit Position(cv::Point, int);
    int getX() const;
    int getY() const;
    int getZ() const;

private:
    cv::Point pos;  // Point représentant la position dans le plan
    int z;          // Profondeur dans l'espace
};

std::ostream& operator<< (std::ostream&, const Position&);


#endif //PEDESTRIAN_DETECTOR_POSITION_H
