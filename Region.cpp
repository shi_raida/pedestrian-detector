/**
 * @author: raida <raida@crans.org>
 * @date:   Creation: 09/12/2018
 *          Last update: 30/04/2019
 */

#include "Region.h"

using namespace std;
using namespace cv;


Region::Region() {
    this->points = vector<Point>();
};


/**
 * Combine deux régions ensemble
 * @param r1 [Region]: Première région
 * @param r2 [Region]: Seconde région
 */
Region::Region(Region r1, Region r2) {
    this->points = r1.points;
    this->points.insert(this->points.end(), r2.points.begin(), r2.points.end());
}


Region::~Region() {
    this->points.clear();
}


/**
 * Ajoute un pixel dans la Region.
 *
 * @param point [Point] : Pixel à ajouter
 */
void Region::add(Point point) {
    this->points.push_back(point);
}


/**
 * @return [Point] : Centre de la zone
 */
Point Region::center() {
    return (this->zone().br() + this->zone().tl()) / 2;
}


/**
 * @return [size_t] : Nombre de pixels dans la région
 */
size_t Region::size() {
    return this->points.size();
}


/**
 * @return [Rect] : Plus petit rectangle contenant la région
 */
Rect Region::zone() {
    if (this->_zone.empty()) this->initZone();
    return this->_zone;
}


/**
 * Calcule les coordonnées du plus petit rectangle contenant la région
 */
void Region::initZone() {
    float minX(MAXFLOAT), maxX(-1), minY(MAXFLOAT), maxY(-1);
    for(int i = 0; i < this->points.size(); i++) {
        if(points[i].x < minX)
            minX = points[i].x;
        if(points[i].x > maxX)
            maxX = points[i].x;
        if(points[i].y < minY)
            minY = points[i].y;
        if(points[i].y > maxY)
            maxY = points[i].y;
    }
    this->_zone = Rect(Point((int)maxX, (int)maxY), Point((int)minX, (int)minY));
}