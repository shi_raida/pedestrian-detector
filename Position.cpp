/**
 * @author: raida <raida@crans.org>
 * @date:   Creation: 11/01/2019
 *          Last update: 28/04/2019
 */

#include <utility>
#include "Position.h"

using namespace std;
using namespace cv;


/**
 * @param pos [Point]: Point représentant la position dans le plan
 * @param z [int]: Profondeur dans l'espace
 */
Position::Position(Point pos, int z) {
    this->pos = std::move(pos);
    this->z = z;
}


/**
 * @return [int]: Position horizontale
 */
int Position::getX() const {
    return this->pos.x;
}


/**
 * @return [int]: Position verticale
 */
int Position::getY() const {
    return this->pos.y;
}


/**
 * @return [int]: Profondeur
 */
int Position::getZ() const {
    return this->z;
}


/**
 * @param out [ostream&]: Flux de données
 * @param position [Position&]: Position à afficher
 *
 * @return [ostream&]: Flux de données afin de chainer
 */
ostream& operator<< (ostream& out, const Position& position) {
    return out << '(' << position.getX() << ", " << position.getY() << ", " << position.getZ() << ')';
}