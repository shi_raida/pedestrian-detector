/**
 * @author: raida <raida@crans.org>
 * @date:   Creation: 09/12/2018
 *          Last update: 30/04/2019
 */

#ifndef PEDESTRIAN_DETECTOR_REGION_H
#define PEDESTRIAN_DETECTOR_REGION_H

#include <iostream>
#include <vector>
#include "opencv2/opencv.hpp"

#ifndef MAXFLOAT
#define MAXFLOAT 10000
#endif


/**
 * Représente une région de pixels sur une image.
 *
 * @param center [Point] : Centre de la zone
 * @param size [size_t] : Nombre de pixels dans la région
 * @param zone [Rect] : Plus petit rectangle contenant la région
 */
class Region {
public:
    Region();
    Region(Region, Region);
    ~Region();
    void add(cv::Point);
    cv::Point center();
    size_t size();
    cv::Rect zone();


private:
    std::vector<cv::Point> points;
    cv::Rect _zone;

    void initZone();
};


#endif //PEDESTRIAN_DETECTOR_REGION_H
